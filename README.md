![Hoppr Docs Banner](static/repo-media/hoppr-docs-banner.png)

Hoppr.dev utilizes the [Docusaurus 2](https://tutorial.docusaurus.io/docs/intro) framework.
For simple documentation edits, use the `Edit this page` link at the bottom of a live page.  For more complex changes, the Docusaurus tutorial above and some basic React experience will help.

### Installation

```
$ yarn
```

### Local Development

```
$ yarn start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

### Build

```
$ yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.