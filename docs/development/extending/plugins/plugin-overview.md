---
sidebar_position: 1
title: Overview
---

# Plugin Overview

Each [Hoppr plugin](https://gitlab.com/hoppr/hoppr/-/blob/dev/hoppr/base_plugins/hoppr.py#L2) runs one or more of the following methods, in order:

- `pre_stage_process` is run before any component based processing.  Initialization should be performed in this method.
- `process_component` is run on each component of the consolidated BOM.  This process may be limited to run only on components of one or more specified purl types.
- `post_stage_process` is run after all components have been processed.  Cleanup tasks should be performed here.

## Plugin Types

Plugins are grouped into categories based on their intended purpose.  In most cases, these categories are simply convenient ways to refer to similar plugins, and is not actually reflected in the code.

- [**Collectors**](https://gitlab.com/hoppr/hoppr/-/blob/dev/hoppr/base_plugins/collector.py#L2) pull artifacts from various repositories and store them for later inclusion in bundles
- **Bundlers** take the artifacts copied by the collectors and package them in a bundle of some kind, suitable for transfer
- **Reports** _(coming soon)_ provide summary information about a portion of a Hoppr run
- **Augmenters and Filters** _(coming soon)_ add or remove components or component information in the delivered SBOM