---
sidebar_position: 2
---

# Starter Plugin Template

Clone the [Hoppr Starter Plugin](https://gitlab.com/hoppr/plugins/starter-plugin) as a basis for your first Hoppr plugin:

```bash
[~]$ git clone https://gitlab.com/hoppr/plugins/starter-plugin.git
```

Assuming you have [already installed Hoppr](https://pypi.org/project/hoppr/), execute a `bundle` from your starter plugin directory:

```bash
[starter-plugin]$ hopctl bundle starter_files/manifest.yml --transfer starter_files/transfer.yml --credentials starter_files/credentials.yml
```

This plugin simply logs output to show the [Hoppr processing flow](/docs/using-hoppr/tutorials/processing-101.md) across a complete set of core input files (manifest/transfer/credentials) and how it processes an SBOM.

We strongly recommend that any plugin class inherit from the `HopprPlugin` class ([in the `hoppr.base_plugins.hoppr` module](https://gitlab.com/hoppr/hoppr/-/blob/dev/hoppr/base_plugins/hoppr.py#L2)), which provides the framework for:

1. The `__init__` constructor, which takes two arguments:
    - The Hoppr `context`, which includes information about the current state of the process,
    - (Optional) the `config` to be used (as a Python `dict` object). Since the configuration is specific to each plugin, there are no requirements for its structure. The starter plugin demonstrates use of `config` inside the `pre_stage_process` method.
1. Reporting the version number as a string via the `get_version()` method. This is implemented as an abstract method in the `HopprPlugin` base class, and must be overridden.
1. Methods to handle SBOM processing. Each should return a `Result` object (defined in `hoppr.result`). When sub-classing `HopprPlugin`, each by default returns a `Result` object with the status of `SKIP`, meaning that the process did nothing, and the logs for that process will be deleted. Read more about plugin layout [in the overview](./plugin-overview.md).

:::tip

Make sure to read through the comments and `NOTE:` sections in the Starter Plugin's `plugin.py` to get a better feel for how a plugin operates!

:::
