---
sidebar_position: 1
title: Reference Guide
---

The following core plugins are included with Hoppr:

### Bundle TAR

|                          |              |                                                                                                                            |
| ------------------------ | ------------ | -------------------------------------------------------------------------------------------------------------------------- |
| **Type**                 |              | Bundler                                                                                                                    |
| **Transfer Config Tag**  |              | hoppr.core_plugins.tar_bundle                                                                                              |
| **Supported Purl Types** |              | _All_                                                                                                                      |
| **Required Commands**    |              | _None_                                                                                                                     |
|                          |              |                                                                                                                            |
| **Config<br/>Options**   | compression  | `none`                                                                                                                     |
|                          |              | `gzip`                                                                                                                     |
|                          |              | `gz` (default)                                                                                                             |
|                          |              | `bzip2`                                                                                                                    |
|                          |              | `bz2`                                                                                                                      |
|                          |              | `lxma`                                                                                                                     |
|                          |              | `xz`                                                                                                                       |
|                          | tarfile_name | String.  Defaults to `bundle.tar`, `bundle.tar.gz`, `bundle.tar.bz2`, or `bundle.tar.xz` in the current working directory. |

Bundles the collected artifacts into a tar-ball with the specified compression.

---

### Collect APT

|                          |             |                                                                                                                                                                                                               |
| ------------------------ | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Type**                 |             | Collector                                                                                                                                                                                                     |
| **Tranfer Config Tag**   |             | hoppr.core_plugins.collect_apt_plugin                                                                                                                                                                         |
| **Supported Purl Types** |             | `deb`                                                                                                                                                                                                         |
| **Required Commands**    |             | `apt`, `apt-cache`                                                                                                                                                                                                           |
| **Config<br/>Options**   |  | _None_ |

Collects APT Packages.

---

### Collect DNF

|                          |             |                                                                                                                                                                                                               |
| ------------------------ | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Type**                 |             | Collector                                                                                                                                                                                                     |
| **Tranfer Config Tag**   |             | hoppr.core_plugins.collect_dnf_plugin                                                                                                                                                                         |
| **Supported Purl Types** |             | `rpm`                                                                                                                                                                                                         |
| **Required Commands**    |             | `dnf`                                                                                                                                                                                                           |
| **Config<br/>Options**   | dnf_command | String.  Defaults to `dnf`.  Command to be used to execute dnf commands.  May be overridden with another command that uses the same syntax, or a fully qualified path if `dnf` is not on the default `$PATH`. |

Collects DNF/YUM Packages.

---

### Collect Docker

|                          |                |                                                                                                                                                                                                                        |
| ------------------------ | -------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Type**                 |                | Collector                                                                                                                                                                                                              |
| **Transfer Config Tag**  |                | hoppr.core_plugins.collect_docker_plugin                                                                                                                                                                               |
| **Supported Purl Types** |                | `docker`                                                                                                                                                                                                               |
| **Required Commands**    |                | `skopeo`                                                                                                                                                                                                               |
| **Config<br/>Options**   | skopeo_command | String.  Defaults to `skopeo`.  Command to be used to execute skopeo commands.  May be overridden with another command that uses the same syntax, or a fully qualified path if `skopeo` is not on the default `$PATH`. |

Collects Docker Images

---

### Collect GIT

|                          |             |                                                                                                                                                                                                               |
| ------------------------ | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Type**                 |             | Collector                                                                                                                                                                                                     |
| **Tranfer Config Tag**   |             | hoppr.core_plugins.collect_git_plugin                                                                                                                                                                         |
| **Supported Purl Types** |             | `git`, `gitlab`, `github`                                                                                                                                                                                     |
| **Required Commands**    |             | `git`                                                                                                                                                                                                         |
| **Config<br/>Options**   | git_command | String.  Defaults to `git`.  Command to be used to execute git commands.  May be overridden with another command that uses the same syntax, or a fully qualified path if `git` is not on the default `$PATH`. |

Collects git repositories.  This plugin collects the full repository, all branches and tags.

---

### Collect Helm

|                          |              |                                                                                                                                                                                                                  |
| ------------------------ | ------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Type**                 |              | Collector                                                                                                                                                                                                        |
| **Transfer Config Tag**  |              | hoppr.core_plugins.collect_dnf_plugin                                                                                                                                                                            |
| **Supported Purl Types** |              | `helm`                                                                                                                                                                                                           |
| **Required Commands**    |              | `helm`                                                                                                                                                                                                           |
| **Config<br/>Options**   | helm_command | String.  Defaults to `helm`.  Command to be used to execute helm commands.  May be overridden with another command that uses the same syntax, or a fully qualified path if `helm` is not on the default `$PATH`. |

Collects Helm charts.

---

### Collect Maven

|                          |               |                                                                                                                                                                                                               |
| ------------------------ | ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Type**                 |               | Collector                                                                                                                                                                                                     |
| **Transfer Config Tag**  |               | hoppr.core_plugins.collect_maven_plugin                                                                                                                                                                       |
| **Supported Purl Types** |               | `maven`                                                                                                                                                                                                       |
| **Required Commands**    |               | `mvn`                                                                                                                                                                                                         |
| **Config<br/>Options**   | maven_command | String.  Defaults to `mvn`.  Command to be used to execute mvn commands.  May be overridden with another command that uses the same syntax, or a fully qualified path if `mvn` is not on the default `$PATH`. |
|                          | maven_opts    | Array of Strings.  Additional options to be passed to the mvn command                                                                                                                                         |

Collects Maven artifacts.  Note that the `mvn` command does not recognize the usual proxy environment variables, so any proxy will need to be specified in the `maven_opts`, for example:

```yml
    plugins:
      - name: hoppr.core_plugins.collect_maven_plugin
        config:
          maven_opts:
            - -Dhttp.proxyHost=proxy.mycompany.com
            - -Dhttp.proxyPort=80
            - -Dhttps.proxyHost=proxy.mycompany.com
            - -Dhttps.proxyPort=80
            - -Dhttp.nonProxyHosts=*.mycompany.com```
```

---

### Collect Nexus Search

| | | |
| --- | --- | --- |
| **Type** | | Collector |
| **Transfer Config Tag**  | | hoppr.core_plugins.collect_nexus_search |
| **Supported Purl Types** | | By default, all types _except_ 'git' `gitlab`, `github` |
| **Required Commands**    | | _None_ |
| **Config<br/>Options**   | purl_types | Array of Strings.  Purl types to be processed by this plugin |

Collects artifacts from a Nexus instance.  Will search all repositories in any entry in the corresponding manifest `repositories` list that is recognized as a Nexus instance for the specified component, and choose the most recent match (by version number).

Note that the `purl_types` option should be included if there are other collectors included in the `transfer` file.  Otherwise any artifact covered by those collectors will be processed twice, once by the native collector, and once by the Nexus Search collector.

```yml
    plugins:
      - name: hoppr.core_plugins.collect_dnf_plugin
      - name: hoppr.core_plugins.collect_nexus_search
        config:
          purl_types:
            - git
            - maven
            - generic
```

---

### Collect PyPI

|                          |             |                                                                                                                                                                                                                                     |
| ------------------------ | ----------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Type**                 |             | Collector                                                                                                                                                                                                                           |
| **Transfer Config Tag**  |             | hoppr.core_plugins.collect_pypi_plugin                                                                                                                                                                                              |
| **Supported Purl Types** |             | `pip`, `pypi`                                                                                                                                                                                                                       |
| **Required Commands**    |             | _None_                                                                                                                                                                                                                              |
| **Config Options**       |             | _None_                                                                                                                                                                                                                              |

Collects Pypi Packages.

---

### Collect Raw

|                          |     |                                       |
| ------------------------ | --- | ------------------------------------- |
| **Type**                 |     | Collector                             |
| **Transfer Config Tag**  |     | hoppr.core_plugins.collect_raw_plugin |
| **Supported Purl Types** |     | `binary`, `generic`, `raw`            |
| **Required Commands**    |     | _None_                                |
| **Config Options**       |     | _None_                                |

Collects artifacts that are not managed by a package manager.  For example, local files, URL downloads.

To collect local files, use `file:` as the repository url in the manifest file.  For URL downloads, the url will be a concatination of the repository url and the purl namespace, followed by the purl name.

---

### Composite Collector

|                          |     |                                       |
| ------------------------ | --- | ------------------------------------- |
| **Type**                 |     | Collector                             |
| **Transfer Config Tag**  |     | hoppr.core_plugins.composite_collector |
| **Supported Purl Types** |     | _All_ (see below)            |
| **Required Commands**    |     | _None_                                |
| **Config Options**       | plugins    | Array of plugins to be used (Required)                               |

Collects artifacts by trying multiple collectors in sequence.  For example, if an RPM artifact is not found by the DNF collector, 
we can try the Nexus Search collector as a backup.

At least one plugin must be specified in the config of a Composite Collector, and two must be specified to be really useful.  For example:

```yml
stages:
  Collect:
    plugins:
    - name: "hoppr.core_plugins.collect_helm_plugin"
    - name: "hoppr.core_plugins.composite_collector"
      config:
        plugins: 
        - name: "hoppr.core_plugins.collect_dnf_plugin"
        - name: "hoppr.core_plugins.collect_nexus_search"
```

Each child plugin may optionally have its own config block.

The composite collector is supports the purl types supported by the first child plugin listed (in the above example, `collect_dnf_plugin`).

### Delta SBOM

|                          |              |                                                                                                                            |
| ------------------------ | ------------ | -------------------------------------------------------------------------------------------------------------------------- |
| **Type**                 |              | Filter                                                                                                                    |
| **Transfer Config Tag**  |              | hoppr.core_plugins.delta_sbom                                                                                              |
| **Supported Purl Types** |              | _All_                                                                                                                      |
| **Required Commands**    |              | _None_                                                                                                                     |
|                          |              |                                                                                                                            |
| **Config<br/>Options**   | previous | String.  Location of a file (either a manifest or a tar bundle) describing the previous delivery. |

Filters the SBOM so include only those components that were not previously bundled are included.

The components to be excluded are identified by specifying the previous delivery, either with the previous` config option or the `-pd`/`--previous-delivery` command line option.
* The `-pd`/`--previous-delivery` command line option overrides the`previous` config option, if present.

* If no `previous` option is set (either in the config or on the command line), all components will be included in the delivered SBOM.

* If there is no difference between the previous SBOM and the current SBOM, the plug-in fails (there is no use in continuing to build a bundle with no components).

To determine which components to include, each "new" component is compared to all "previous" components.

* A component is considered to have been previously delivered if the previous SBOM includes a component with the same purl (including all qualifiers), and if all hashes that are present in both SBOMs for that component match.

* If no hashes can be checked (for example, one or the other SBOM has no hashes, or one SBOM has only a SHA-1 hash, and the other only a SHA-256), the comparison is done on purl value alone, including all qualifiers.

* A special rule applies when purls match, no hash comparisons are available, and the version number is either missing or `latest`.  In this case, the components are considered _not_ matching, and the new component will be included.  This is to allow for the fact that there is no way to determine whether or not a non-version-controlled artifact has been updated.

If a `transfer.config` file does _not_ include any stages that run this plug-in, such a stage will be prepended to those in the `tranfer.config` file.  In this case, no `previous` location will be included in the `transfer` structure, and no comparisons will be made unless a  `-pd`/`--previous-delivery` command line option is specified.

---
