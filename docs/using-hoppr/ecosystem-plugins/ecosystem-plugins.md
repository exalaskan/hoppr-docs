---
sidebar_position: 1
title: Reference Guide
---

Hoppr has a growing ecosystem of plugins [available on GitLab](https://gitlab.com/hoppr/plugins) and [beyond](https://github.com/lmco/hoppr-cop).  If you'd like to integrate your plugin docs with our ecosystem documentation, please see more on the [contribution process here](/docs/category/contribute-to-hoppr).