---
sidebar_position: 1
---

# Definitions

## Input Files

| Term          | Definition |
| ------------- | ---------- |
| `Credentials` | A Hoppr input file that defines what environment variable credentials map to URIs for downloading Manifests, SBOMs, and Components. [Credentials Documentation](/docs/using-hoppr/tutorials/processing-101.md#credentials) |
| `Manifest`    | A Hoppr input file that defines the "what" and "where" to process and transfer. [Manifest Documentation](/docs/using-hoppr/tutorials/processing-101.md#manifests) |
| `Transfer`    | A Hoppr input file that defines the "processing" that occurs as part of the transfer through a series of stages and plugins. [Transfer Documentation](/docs/using-hoppr/tutorials/processing-101.md#transfers) |

## Hoppr Vernacular

| Term            | Definition |
| --------------- | ---------- |
| `Combined SBOM` | SBOM object for the bundle, showing the data and information for what is bundled for transfer. |
| `SBOM`          | Software Bill of Material as defined by the [CycloneDX Specification](https://cyclonedx.org/specification/overview) |
