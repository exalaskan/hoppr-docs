---
sidebar_position: 1
---

# What Is Hoppr?

**Hoppr** is your **software bill-of-materials (SBOM)** and **secure software supply-chain (S3C) ** utility kit.  Built on a simple plugin architecture, Hoppr can collect, process, and bundle your digital assets to streamline transfers from one production environment to another.  Whether on cloud-connected or airgapped networks, Hoppr can handle it.

<div style={{ textAlign: 'center' }}>
  <img style={{ maxHeight: '300px'}} src="/img/hoppr-components/no-text/Components-NoText.png" />
</div>

**A few of the many possibilities:**

- Collect, bundle, and deploy artifacts as [defined by a manifest and collection of SBOMs](/docs/using-hoppr/tutorials/processing-101.md)
- Release products to be consumed by other teams through a manifest
- Extend to provide further information on the contents of the transfer

## Goals

- `Collect`
  - _Framework to collect digital assets and build dependencies for consolidated packaging_
- `Process`
  - _Secure Software Supply Chain Management of these dependencies_
- `Bundle`
  - _Consolidated packages grouped into repeatable bundles_

## Key Features

- `Standardized workflows`
  - _Repeatable collection and packaging of applications, libraries, and artifacts_
- [Infinitely extendable with plugins](/docs/development/start-here.mdx)
  - _Designed with extensibility in mind, Hoppr encourages the development of additional plugins for added functionality_
- [Batteries-included with core plugins](/docs/using-hoppr/core-plugins/core-plugin-reference.md)
  - _Collect, process, bundle common component types_

## Developing for Hoppr

Learn more about Hoppr development at our [Development Center](/docs/development/start-here.mdx)

## License

Hoppr is [MIT Licensed](https://gitlab.com/hoppr/hoppr/-/raw/main/LICENSE).
