---
sidebar_position: 1
---

# Release Process

## Versioning

Hoppr uses trunk based development paired with semantic releases. All merges
to [main](https://gitlab.com/hoppr/hoppr/-/tree/main) result in a new release.

The versioning of `major.minor.patch` is handled with [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)
and [semantic-release](https://github.com/semantic-release/semantic-release) in the GitLab pipeline.

## Releasing to PyPI

Hoppr releases are pushed to [pypi.org](https://pypi.org/) outside of this project. A separate pipeline completes outside validation, builds, and deploys the [WHL package](https://pypi.org/project/hoppr/#history)
